from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.uix.listview import ListItemButton
from kivy.uix.popup import Popup


class LocationButton(ListItemButton):
	pass

class WeatherRoot(Popup):
	pass


class BodyApp(BoxLayout):
	search_input = ObjectProperty()
	find_sexy_girls = ObjectProperty()
	current_woman = ObjectProperty()
	find = ""
	yes = 0

	women_list = {
		"Jang Na Ra":{'avatar':'img/jang_na_ra2.jpg', 'date_b':'March-18-1981', 'age':'35 years old', 'p_birth':'Seoul, South Korea', 'proffession':'Actriz & Singer', 'zsign':'Pisces', 'status':'Single'},
		"Bae Su Ji":{'avatar':'img/suzy.jpg', 'date_b':'Octuber-10-1994', 'age':'21 years old', 'p_birth':'Kwangju, South Korea', 'proffession':'Actriz, Singer, Model & MC', 'zsign':'Libra', 'status':'Single'},
		"Jung So Min":{'avatar':'img/Jung_So_Min.jpg', 'date_b':'March-16-1989', 'age':'27 years old', 'p_birth':'South Korea', 'proffession':'Actriz & Model', 'zsign':'Pisces', 'status':'Single'},
		"UI":{'avatar':'img/UI.jpg', 'date_b':'May-16-1993', 'age':'22 years old', 'p_birth':'Seoul, South Korea', 'proffession':'Actriz & Singer', 'zsign':'Taurus', 'status':'Single'},
		"Lee Min Jung":{'avatar':'img/Lee_Min_Jung.jpg', 'date_b':'February-16-1983', 'age':'33 years old', 'p_birth':'Incheon, South Korea', 'proffession':'Actriz & Model', 'zsign':'Aquarius', 'status':'Married'},
		"Park Min Young":{'avatar':'img/Park_Min_Young.jpg', 'date_b':'March-04-1986', 'age':'30 years old', 'p_birth':'Seoul, South Korea', 'proffession':'Dancer, Actriz & Model', 'zsign':'Pisces', 'status':'Single'},
		"Han Hye Seul":{'avatar':'img/Han_Hye_Seul.jpg', 'date_b':'September-18-1982', 'age':'33 years old', 'p_birth':'Los Angeles, California, E.U.A', 'proffession':'Actriz & Model', 'zsign':'Virgo', 'status':'Single'},
		"Jin Ye Sol":{'avatar':'img/Jin_Ye_Sol.jpg', 'date_b':'September-24-1985', 'age':'30 years old', 'p_birth':'Seoul, South Korea', 'proffession':'Actriz', 'zsign':'Libra', 'status':'Single'},
		"Park Shin Hye":{'avatar':'img/Park_Shin_Hye.jpg', 'date_b':'February-18-1990', 'age':'26 years old', 'p_birth':'Gwangju, South Korea', 'proffession':'Actriz, Model, Singer & Dancer', 'zsign':'Aquarius', 'status':'Single'},
	}

	def MyGirls(self):
		self.find_sexy_girls.item_strings = self.women_list.keys()

	def show_all_women(self):
		self.search_message.text = "Search your woman"
		self.find_sexy_girls.adapter.data[:]
		self.MyGirls()

	def search_woman(self):
		self.find = "{}".format(self.search_input.text)
		self.found_faithful(self.find)

	def show_current_woman(self, faithful = None):
		if faithful is not None:
			self.current_woman = WeatherRoot()
			for name_woman in self.women_list.keys():
				if name_woman == faithful:
					self.current_woman.faithful = faithful
					self.current_woman.avatar = self.women_list[name_woman]["avatar"]
					self.current_woman.date_b = self.women_list[name_woman]["date_b"]
					self.current_woman.age = self.women_list[name_woman]["age"]
					self.current_woman.p_birth = self.women_list[name_woman]["p_birth"]
					self.current_woman.proffession = self.women_list[name_woman]["proffession"]
					self.current_woman.zsign = self.women_list[name_woman]["zsign"]
					self.current_woman.status = self.women_list[name_woman]["status"]
			self.current_woman.open()

	def found_faithful(self, data):
		items = []
		if data:
			self.MyGirls()
			for x in xrange(0,len(self.find_sexy_girls.item_strings)):
				if data in self.find_sexy_girls.item_strings[x].lower():
					self.yes = 1
					items.append(self.find_sexy_girls.item_strings[x])

		else:
			self.yes = 0
		if self.yes:
			self.find_sexy_girls.item_strings = []
			self.find_sexy_girls.adapter.data[:]
			self.find_sexy_girls.adapter.data.extend(items)
			self.find_sexy_girls._trigger_reset_populate()

			if len(items) > 1:
				self.yes = 0
				self.search_message.text = "OHH!! It has been found beautiful girls!!   (^).(^)*"
			else:
				self.yes = 0
				self.search_message.text = "OHH!! It has been found one beautiful girl!!   (^).(^)*"
		else:
			self.yes = 0
			self.search_message.text = "The girl "+data+" is not here!!  (v).(v)"
			self.show_all_women()


class AlwaysFaithfulApp(App):
	def build (self):
		girl = BodyApp()
		girl.MyGirls()
		return girl


if __name__ == "__main__":
	AlwaysFaithfulApp().run()
